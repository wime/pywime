"""SOlarized Light"""


colors = {
    'primary_background': '#fdf6e3',
    'secondary_background': '#eee8d5',
    'primary_foreground': '#657b83',
    'secondary_foreground': '#93a1a1',
    'accent1': '#dc322f',
    'accent2': '#268bd2',
    'accent3': '#859900',
    'accent4': '#d33682',
    'accent5': '#b58900',
    'accent6': '#cb4b16',
    'accent7': '#2aa198',
    'accent8': '#6c71c4',
}
