import os
import datetime as dt
import plotly.io as pio
from pywime.colorschemes import solarized_dark, solarized_light


def _get_system_theme():
    theme = os.popen("gsettings get org.gnome.desktop.interface color-scheme")
    theme = theme.read().split("\n")[0]
    theme = theme.replace("'", "")
    theme = theme.split("-")[1]
    return theme


def _colorscheme_type(background, foreground):
    """Finds if the colorscheme is a dark or light scheme"""
    sum_foreground = (
        int(foreground[1:3], 16)
         + int(foreground[3:5], 16) 
         + int(foreground[5:7], 16)
    )
    sum_background = (
        int(background[1:3], 16)
        + int(background[3:5], 16) 
        + int(background[5:7], 16)
    )
    if sum_background > sum_foreground:
        return "light"
    return "dark"


def _make_template(colorscheme: str, colors: dict):
    """
    Make a template based on two background colors, two foreground colors and 
    eight accent colors.

    Parameters
    ----------
    colorscheme: str
        Name of new colorscheme
    colors: dict
        Dictionary with the 12 new colors. They need to be named 
        'primary_background', 'secondary_background', 'primary_foreground', 
        'secondary_foreground' and 'accent1' to 'accent8'.
    """
    colorscheme_type = _colorscheme_type(
        colors["primary_background"],
        colors["primary_foreground"],
    )
    if colorscheme_type == "dark":
        template = pio.templates["plotly_dark"]
    else:
        template = pio.templates["plotly"]

    template.layout["plot_bgcolor"] = colors["secondary_background"]
    template.layout["paper_bgcolor"] = colors["primary_background"]
    template.layout["xaxis"]["gridcolor"] = colors["secondary_foreground"]
    template.layout["xaxis"]["linecolor"] = colors["primary_foreground"]
    template.layout["xaxis"]["zerolinecolor"] = colors["primary_foreground"]
    template.layout["yaxis"]["gridcolor"] = colors["secondary_foreground"]
    template.layout["yaxis"]["linecolor"] = colors["primary_foreground"]
    template.layout["yaxis"]["zerolinecolor"] = colors["primary_foreground"]
    template.layout["font"]["color"] = colors["primary_foreground"]
    template.layout["colorway"] = [
        colors["accent1"],
        colors["accent2"],
        colors["accent3"],
        colors["accent4"],
        colors["accent5"],
        colors["accent6"],
        colors["accent7"],
        colors["accent8"],
    ]

    pio.templates[colorscheme] = template


def custom_templates():
    """Make custom plotly templates"""
    _make_template("solarized_dark", solarized_dark.colors)
    _make_template("solarized_light", solarized_light.colors)


def kitty_show(fig):
    """
    Show plotly in Kitty terminal window

    Arguments
    ---------
        fig : plotly.fig
    """
    # Path
    path = os.environ["HOME"] + '/.temp/'
    if not os.path.exists(path):
        os.mkdir(path)
    filename = dt.datetime.now().strftime("%Y%m%d%H%M%S") + "plot.jpg"
    file = path + filename
    fig.write_image(file)
    os.system(f"kitty + kitten icat {file}")
    os.remove(file)


def set_template():
    custom_templates()
    system_theme = _get_system_theme()
    pio.templates.default = "solarized_" + system_theme
    colors = {'dark': solarized_dark, 'light': solarized_light}
    return colors[system_theme]
